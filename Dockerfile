FROM python:3.13.1-alpine3.19

WORKDIR /app/

# Generic
COPY app /app/

# Python
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /code/

# Build arguments
ARG BUILD_DATE
ARG BUILD_REF

# Labels
LABEL \
    maintainer="Robbert Müller <spam.me@grols.ch>" \
    org.label-schema.description="Container for update docker hub meta data" \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.name="docker-update" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.usage="https://gitlab.com/pipeline-components/docker-update/blob/main/README.md" \
    org.label-schema.vcs-ref=${BUILD_REF} \
    org.label-schema.vcs-url="https://gitlab.com/pipeline-components/docker-update/" \
    org.label-schema.vendor="Pipeline Components"
