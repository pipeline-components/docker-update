# Pipeline Components: Docker update

![Project Stage][project-stage-shield]
![Project Maintenance][maintenance-shield]
[![License][license-shield]](LICENSE)

[![GitLab CI][gitlabci-shield]][gitlabci]

## Docker status

[![Docker Version][version-shield]][microbadger]
[![Docker Layers][layers-shield]][microbadger]
[![Docker Pulls][pulls-shield]][dockerhub]

## Usage

The image is for running a custom application docker-update which will update a repositories description on docker hub.
The image is based on python:3.7-alpine3.8

## Examples

```yaml
docker-update:
  stage: linting
  image: pipelinecomponents/docker-update:latest
  only: master
  script:
    - docker-update 'myorg/myproject' 'short description' README.md
```

Getting the docker description from the label:

```sh
DESC="$(docker inspect myorg/myproject:latest -f '{{ index .Config.Labels "org.label-schema.description" }}')"
```

## Versioning

This project uses [Semantic Versioning][semver] for its version numbering.

## Support

Got questions?

You have several options to get them answered:
todo: addcommunication channel here

You could also [open an issue here][issue]

## Contributing

This is an active open-source project. We are always open to people who want to
use the code or contribute to it.

We've set up a separate document for our [contribution guidelines](CONTRIBUTING.md).

Thank you for being involved! :heart_eyes:

## Authors & contributors

The original setup of this repository is by [Robbert Müller][mjrider].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2023 Robbert Müller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[commits]: https://gitlab.com/pipeline-components/docker-update/commits/main
[contributors]: https://gitlab.com/pipeline-components/docker-update/graphs/main
[dockerhub]: https://hub.docker.com/r/pipelinecomponents/docker-update
[license-shield]: https://img.shields.io/badge/License-MIT-green.svg
[mjrider]: https://gitlab.com/mjrider
[gitlabci-shield]: https://img.shields.io/gitlab/pipeline/pipeline-components/docker-update.svg
[gitlabci]: https://gitlab.com/pipeline-components/docker-update/commits/main
[issue]: https://gitlab.com/pipeline-components/docker-update/issues
[keepchangelog]: http://keepachangelog.com/en/1.0.0/
[layers-shield]: https://images.microbadger.com/badges/image/pipelinecomponents/docker-update.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2025.svg
[microbadger]: https://microbadger.com/images/pipelinecomponents/docker-update
[project-stage-shield]: https://img.shields.io/badge/project%20stage-production%20ready-brightgreen.svg
[pulls-shield]: https://img.shields.io/docker/pulls/pipelinecomponents/docker-update.svg
[releases]: https://gitlab.com/pipeline-components/docker-update/tags
[repository]: https://gitlab.com/pipeline-components/docker-update
[semver]: http://semver.org/spec/v2.0.0.html
[version-shield]: https://images.microbadger.com/badges/version/pipelinecomponents/docker-update.svg

