# -*- encoding: utf-8 -*-
import requests

DOCKER_HUB_API = "https://hub.docker.com/v2/"


class DockerHub:
    """Wrapper to communicate with docker hub API"""

    auth_token = None

    def __init__(self):
        self

    def _do_request(self, url, method="GET", data={}):
        if method not in ["POST", "PATCH"]:
            raise ValueError("Invalid HTTP request method")

        headers = {}
        if self.auth_token:
            headers["Authorization"] = "JWT %s" % self.auth_token

        request_method = getattr(requests, method.lower())
        # pylint: disable=not-callable
        resp = request_method(url, json=data, headers=headers)
        # pylint: enable=not-callable

        content = {}
        if resp.status_code == 200:
            content = resp.json()

        return {"content": content, "code": resp.status_code}

    def login(self, username, password):
        """Dockerhub login

        Arguments:
            username string -- Ducker hub username
            password string -- Docker hub password

        Returns:
            bool -- Login successfull
        """

        data = {"username": username, "password": password}
        resp = self._do_request("%susers/login/" % (DOCKER_HUB_API), "POST", data)

        if resp["code"] == 200:
            self.auth_token = resp["content"]["token"]
        return resp["code"] == 200

    def update(self, repository, short, long):
        """Update repository information on dockerhub

        Arguments:
            repository string -- [description]
            short string -- [description]
            long string -- [description]

        """
        data = {"description": short, "full_description": long}
        resp = self._do_request(
            "%srepositories/%s/" % (DOCKER_HUB_API, repository), "PATCH", data
        )

        return resp["code"] == 200
