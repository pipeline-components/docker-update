#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

from __future__ import print_function
import os
import sys

from dockerhub.api import DockerHub

try:  # for Python 3
    from http.client import HTTPConnection
except ImportError:
    from httplib import HTTPConnection
HTTPConnection.debuglevel = 0


def main():
    fp = open(sys.argv[3], "r")
    content = fp.read()
    fp.close()

    hub = DockerHub()
    if not hub.login(os.getenv("DOCKER_USER"), os.getenv("DOCKER_PASS")):
        print("Aborted, no login")
        sys.exit(1)
    if not hub.update(sys.argv[1], sys.argv[2], content):
        print("Update failed")
        sys.exit(1)


if __name__ == "__main__":
    main()
